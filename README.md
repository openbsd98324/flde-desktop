# flde-desktop

Source code of the cross-platform FLDE environment for Linux, FreeBSD, NetBSD and OpenBSD, using the X11 and FLTK libraries. 

clang or g++ (c++) is needed to compile from source.

sh install.sh will create the files into the directory /usr/local/bin/.


# Screenshot

![](image/flde.png)



# todo

1.) Replace system command by exec in C. 

2.) add tinynohup instead of screen, if needed.






